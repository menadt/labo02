#include <iostream>

using namespace std;

using uint = unsigned int;

// Ejercicio 1

class Rectangulo {
    public:
        Rectangulo(uint alto, uint ancho);
        uint alto();
        uint ancho();
        float area();

    private:
        int alto_;
        int ancho_;

};

Rectangulo::Rectangulo(uint alto, uint ancho) : alto_(alto), ancho_(ancho) {};

uint Rectangulo::alto() {
    return alto_;
};

uint Rectangulo::ancho() {
    return ancho_;
};

float Rectangulo::area(){
    return alto_ * ancho_;
};

// Ejercicio 2

class Elipse {
    public:
        Elipse (uint a, uint b);
        uint r_a();
        uint r_b();
        float area();
        float PI=3.14;

    private:
        uint _ra;
        uint _rb;


};

Elipse::Elipse(uint a, uint b): _ra(a), _rb(b) {}

uint Elipse::r_a() {
    return _ra;
}

uint Elipse::r_b() {
    return _rb;
}

float Elipse:: area(){
    return _ra*_rb*PI;
}

// Ejercicio 3

class Cuadrado {
    public:
        Cuadrado(uint lado);
        uint lado();
        float area();

    private:
        Rectangulo r_;
};

Cuadrado::Cuadrado(uint lado): r_(lado, lado){}

uint Cuadrado::lado() {
    return r_.ancho();
};

float Cuadrado::area() {
    return r_.area();
};

//Ejercicio 4

class Circulo{
    public:
        Circulo(uint radio);
        uint radio();
        float area();

    private:
        Elipse e_;
};

Circulo::Circulo(uint radio): e_(radio, radio){}

uint Circulo:: radio(){
    return e_.r_b();
}

float Circulo::area() {
    return e_.area();
}

// Ejercicio 5

ostream& operator<<(ostream& os, Rectangulo r) {
    os << "Rect(" << r.alto() << ", " << r.ancho() << ")";
    return os;
}

ostream& operator << (ostream& os, Elipse e) {
    os << "Elipse(" << e.r_a() << ", " << e.r_b() << ")";
    return os;
};

// Ejercicio 6
ostream& operator<<(ostream& os, Cuadrado c) {
    os << "Cuad(" << c.lado() << ")";
    return os;
}

ostream& operator<<(ostream& os, Circulo circ) {
    os << "Circ(" << circ.radio() << ")";
    return os;
}