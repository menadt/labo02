#include <iostream>
#include <string>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha (int mes, int dia);
    int mes();
    int dia();
    bool operator==(Fecha o);
    void incrementar_dia();
  private:
    int _mes;
    int _dia;
    //Completar miembros internos
};

Fecha:: Fecha(int mes, int dia): _dia(dia), _mes(mes){};

int Fecha::dia() {
    return _dia;
}

int Fecha::mes() {
    return _mes;
}

ostream& operator<<(ostream& os, Fecha fecha) {
    os << fecha.dia() << "/" << fecha.mes();
    return os;
}

bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    igual_dia = igual_dia && this->mes() == o.mes();
    return igual_dia;
}

void Fecha::incrementar_dia(){
    if (dias_en_mes(_mes)==_dia){
        _dia=1;
        if (_mes<12){
            _mes++;
        }else _mes=1;
    }else _dia++;
}

// Ejercicio 11, 12

class Horario{
    public:
        Horario(uint hora, uint min);
        uint hora();
        uint min();
        bool operator==(Horario hora);
        bool operator<(Horario hora);


private:
        uint _hora;
        uint _min;
};

Horario::Horario(uint hora, uint min): _hora(hora), _min(min) {}

uint Horario::hora() {
    return _hora;
}

uint Horario::min() {
    return _min;
}
bool Horario::operator==(Horario ho) {
    bool igual_hora = this->hora() == ho.hora();
    bool igual_min = this->min() == ho.min();
    return igual_min && igual_hora;
}

ostream& operator<<(ostream& os, Horario horario) {
    os << horario.hora() << ":" << horario.min();
    return os;
}

bool Horario::operator<(Horario ho) {
    bool res;
    if (this->hora() < ho.hora()) {
        res = true;
    } else {
        if (this->hora() == ho.hora() && this->min() < ho.min()) {
            res = true;
        } else res = false;
    }
    return res;
}
// Ejercicio 13

class Recordatorio{
    public:
        Recordatorio(Fecha f, Horario h, string m);
        Fecha fecha();
        Horario horario();
        string mensaje();

    private:
        Fecha _fecha;
        Horario _hora;
        string _mensaje;
};

Recordatorio::Recordatorio(Fecha fecha, Horario horario, std::string mensaje): _fecha(fecha.mes(), fecha.dia()),
                                                                               _hora(horario.hora(), horario.min()), _mensaje(mensaje){}

Horario Recordatorio::horario() {
    return _hora;
}

Fecha Recordatorio::fecha() {
    return _fecha;
}

string Recordatorio::mensaje() {
    return _mensaje;
}


ostream& operator<<(ostream& os, Recordatorio recordatorio) {
    os << recordatorio.mensaje() << " @ " << recordatorio.fecha() << " " << recordatorio.horario();
    return os;
}

// Ejercicio 14

class Agenda {
    public:
        Agenda(Fecha fecha_inicial);
        void agregar_recordatorio(Recordatorio rec);
        void incrementar_dia();
        list<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();

    private:
        Fecha _hoy;
        list<Recordatorio> _recordatorios;
};

Agenda::Agenda(Fecha fecha_inicial):_recordatorios(), _hoy(fecha_inicial) {}

void Agenda::incrementar_dia() {
    _hoy.incrementar_dia();
}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    _recordatorios.push_back(rec);
}

Fecha Agenda::hoy() {
    return _hoy;
}

void ordenaryagregarRecordatorios(list<Recordatorio>& record, Recordatorio r ){
    if (record.back().horario()<r.horario()){
        record.push_back(r);
    }else {
        list<Recordatorio>::iterator pos;
        pos = record.begin();
        while (pos != record.end()) {
            if (!(pos->horario() < r.horario())) {
                record.insert(pos, r);
            }
            pos++;
        }
    }
}
list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list <Recordatorio> res;
    for (Recordatorio r:_recordatorios){
        if (r.fecha()==hoy()){
            if (res.size()==0){
                res.push_back(r);
            }else{
                ordenaryagregarRecordatorios(res,r);
                }
            }
        }
    return res;
}

/*list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list <Recordatorio> res;
    for (Recordatorio r: _recordatorios) {
        if (r.fecha() == hoy()) {
            res.push_back(r);
        }
    }
    res.sort([](Recordatorio r1,Recordatorio r2){
        return r1.horario() < r2.horario();
    });
    return res;
}*/

ostream& operator<<(ostream& os, Agenda agenda) {
    os << agenda.hoy().dia() << "/" << agenda.hoy().mes() << endl;
    os << "=====" <<endl;
    for (Recordatorio r:agenda.recordatorios_de_hoy()){
        os << r << endl;
    }
    return os;
}

